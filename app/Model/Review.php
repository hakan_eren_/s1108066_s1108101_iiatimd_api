<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Trainingen;

class Review extends Model
{
    public function product(){
      return $this->belongsTo(Trainingen::class);
    }
}
