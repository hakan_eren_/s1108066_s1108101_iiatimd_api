<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model\Trainingen;
use Faker\Generator as Faker;

$factory->define(Trainingen::class, function (Faker $faker) {
  $trainingg = [
      'Deadlift',
      'Pushup',
      'Jumping-Jacks',
      'Pull-Up',
      'Squat',
      'Pull-Down',
      "Leg-Press",
      "Hardend-Trap",
      "Tricerty",
      "Curl",
      "Crunches"
  ];
    return [
        'name' => $trainingg[rand(0, count($trainingg) - 1)],
        'bijnaam' => $faker->paragraph,
        'calorie' => $faker->numberBetween(100,1000),
        'moeilijkheidsgraad' => $faker->randomDigit,
        'waarde' => $faker->numberBetween(2,30)
    ];
});
