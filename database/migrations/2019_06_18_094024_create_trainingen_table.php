<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrainingenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainingens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('bijnaam');
            $table->integer('calorie');
            $table->integer('moeilijkheidsgraad');
            $table->integer('waarde');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Trainingen');
    }
}
